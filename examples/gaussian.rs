extern crate mentat;
extern crate matplotrust;

use mentat::Normal;
use matplotrust::{Figure, line_plot};

fn main() {
    let mut normal = Normal::new(0.0, 1.0);
    
    let mut ys: Vec<f64> = Vec::new();

    let xs = 0..1000;

    for x in xs {
        ys.push(normal.sample());
    } 

    let mut figure = Figure::new();
    let xs = (0..1000).collect();
    let lp = line_plot::<i32, f64>(xs, ys, None);
    figure.add_plot(lp.clone());
    figure.save("/tmp/random_walk.png")

}
